import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  /*
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Temperature(),
    );
  }
  */
  App createState() => App();
}

final temperature_Controller = TextEditingController();
final mask_Controller = TextEditingController();

class App extends State<MyApp> with TickerProviderStateMixin {
  int selected = 0;
  String title = "";
  final List<Widget> _widgetOptions = <Widget>[
    Center(
      child: Text("first page"),
    ),
    Center(
        child: Container(
      width: 1500,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(30),
              child: TextField(
                controller: temperature_Controller,
                keyboardType: TextInputType.number,
                maxLength: 4,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Student's temperature"),
              ),
            ),
          ),
          Expanded(
            child: Text(" °C"),
          )
        ],
      ),
    )),
    Center(
        child: Container(
      width: 1500,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(30),
              child: TextField(
                controller: mask_Controller,
                keyboardType: TextInputType.number,
                maxLength: 4,
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: "mask quantity"),
              ),
            ),
          ),
          Expanded(
            child: Text(" mask"),
          )
        ],
      ),
    ))
  ];
  final all_tab = [
    Tab(
        text: "Home",
        icon: Icon(
          Icons.home,
        )),
    Tab(
      text: "Temperature",
    ),
    Tab(text: "mask"),
  ];
  TabController controller_tab;
  @override
  void dispose() {
    temperature_Controller.dispose();
  }

  void send() {
    //print(context);
    print("Sented");
    print("Temperature:" + temperature_Controller.text);
    print("Mask quantity:" + mask_Controller.text);
    /*
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: Text("hi"),
              content: Text("hi"),
            ));*/
  }

  void _select(int index, BuildContext context) {
    setState(() {
      if (index == 0) {
        if (selected == 1 || selected == 2) {
          selected -= 1;
        }
      } else if (index == 1) {
        send();
      } else if (index == 2) {
        if (selected == 0 || selected == 1) {
          selected += 1;
        }
      }
    });
    //print(selected);
  }

  @override
  void initState() {
    super.initState();
    controller_tab = TabController(length: all_tab.length, vsync: this);
    controller_tab.addListener(on_pressed);
  }

  void on_pressed() {
    setState(() {
      selected = controller_tab.index;
      print(selected);
    });
  }

  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("Insert temperature"),
        bottom: TabBar(
          tabs: [
            Tab(
                text: "Home",
                icon: Icon(
                  Icons.home,
                )),
            Tab(
              text: "Temperature",
            ),
            Tab(text: "mask"),
          ],
          controller: controller_tab,
        ),
      ),
      body: _widgetOptions.elementAt(selected),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.chevron_left), title: Text("Back")),
          BottomNavigationBarItem(icon: Icon(Icons.send), title: Text("Send")),
          BottomNavigationBarItem(
              icon: Icon(Icons.chevron_right), title: Text("Forward"))
        ],
        currentIndex: selected,
        onTap: (selected) {
          _select(selected, context);
        },
        selectedItemColor: Colors.white,
        backgroundColor: Colors.lightBlue,
        unselectedItemColor: Colors.white,
        selectedFontSize: 15,
        unselectedFontSize: 15,
      ),
    ));
  }
}
